import { Speech } from 'expo';
import { Audio } from 'expo';


const TIMERINTERVALMS = 250;
const SILENCEMP3 = require("./assets/silence.mp3");

class DuckingSpeech {

    static async say(words, duckLeadTimeMillis = 500) {
        await this._prepareDuckSound();
        await this._startDuckSound();
        await this._sleep(duckLeadTimeMillis);
        Speech.speak(words, {})
        this._startHeartbeat(); //We don't get an event when speech finishes. Instead we have to poll :(.
    }

    static _startHeartbeat() {
        if (!this._isHeartBeating()) {
            this.timer = setInterval(this._heartbeat.bind(this),TIMERINTERVALMS);
        }
    }

    static _isHeartBeating() {
        return !!this.timer; 
    }

    static _stopHeartbeat() {
        if (this._isHeartBeating()) {
            clearInterval(this.timer);
            this.timer = null;
        }
    }

    static async _heartbeat() {
        var speechStatus = await Speech.isSpeakingAsync();
        if (!speechStatus) {
            this._stopHeartbeat();
            this._stopDuckSound();
        }
    }

    static async _prepareDuckSound() {
        if (DuckingSpeech.duckSound === undefined) {
            var results = await Audio.Sound.createAsync(SILENCEMP3,{ isLooping: true });
            DuckingSpeech.duckSound = results.sound;
        } 
    }

    static async _startDuckSound() {
        await DuckingSpeech.duckSound.playAsync();
    }

    static async _stopDuckSound() {
        DuckingSpeech.duckSound.stopAsync();
    }

    static _sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

}

export default DuckingSpeech;

