# ducking-speech

Used with Expo to perform text-to-speech, ducking the audio behind it. We play a silent sound in order to cue the OS to duck, because TTS doesn't duck. 

Call it with DuckingSpeech.say(words)

